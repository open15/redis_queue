<?php

namespace App\Http\Controllers;

use App\Services\SendMessage;
use App\Http\Requests\MailRequest;

class MailController extends Controller
{
    public function mailSend(MailRequest $request) {

        $ids = $request->get('ids');
        $message = $request->get('message');

        SendMessage::sendMails($ids, $message);

        return 'Sent';
    }
}
