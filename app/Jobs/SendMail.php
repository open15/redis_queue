<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Redis;

class SendMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id;

    private $message;

    private $url;

    public $tries = 4;

    /**
     * Create a new job instance.
     *
     * @param $id
     * @param $message
     * @param $url
     */
    public function __construct($id, $message, $url)
    {
        $this->id = $id;
        $this->message = $message;
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @param Client $httpClient
     * @return void
     * @throws \Illuminate\Contracts\Redis\LimiterTimeoutException
     */
    public function handle(Client $httpClient)
    {

        // Allow only 2 emails every 1 second
        Redis::throttle('any_key')->allow(2)->every(1)->then(function () use ($httpClient) {


            try {
                $response = $httpClient->post($this->url, [
                    'json' => [
                        'to' => $this->id,
                        'message' => $this->message,
                    ],
                    'http_errors' => false,
                ]);

                if ($response->getStatusCode() == "500") {
                    throw new \Exception('Service temporarily unavailable');
                }

            } catch (\Throwable $exception) {
                if ($this->attempts() > 3) {
                    // hard fail after 3 attempts
                    throw $exception;
                }

                // requeue this job to be executes
                // in 3 minutes (180 seconds) from now
                $this->release(180);
            }

        }, function () {
            // Could not obtain lock; this job will be re-queued
            return $this->release(2);
        });
    }
}
