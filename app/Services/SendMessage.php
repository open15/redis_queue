<?php


namespace App\Services;

use App\Jobs\SendMail;

class SendMessage
{
    const SEND_TO_URL = 'https://send.message/api/send';

    /**
     * @param array $ids
     * @param string $message
     */
    public static function sendMails(array $ids, string $message) {

        foreach ($ids as $id) {
            SendMail::dispatch($id, $message, self::SEND_TO_URL)->onQueue('mail');
        }
    }
}
