## Installation Project

        1. Clone the project to parent directory
        
                $ git clone https://gitlab.com/open15/redis_queue.git
        2. Go to project
        
                $ cd /redis_queue
        3. Init project
        
                $ composer i
        4. Run project

                $ redis-server
                $ php artisan serve
